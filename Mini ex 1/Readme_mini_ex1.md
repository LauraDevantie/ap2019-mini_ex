# Ap2019-Mini_ex
![screenshot](p5/p5/Screenshot mini ex1.png)

**Describe your first independent coding process (in relation to thinking, reading, copying, modifying, writing, uploading, sharing, commenting code)**
The start of my first coding experience was filled with excitement but also frustration. 
Learning to read and trying to understand the coding language, was difficult but also fun. 
I started out coding the Square. After playing around with circles and a Square i started exploring colors and rotation. 
The Square in the middle caused a lot of frustrations when it came to the rotation.  To overcome this struggle I tried several times and suddenly it worked. 
This success gave me energy to try new and different things- in this case, getting the Square to follow the mouse and change random colors. 
All in all was my first experiment very successful, not because i created something very special, 
but because i overcame my struggles and still feel some excitement about coding in the future. 

**How your coding process is differ or similar to reading and writing text? (You may also reflect upon Annette Vee's text on coding literacy)**
My coding process was some what similar to learning to read and write. 
With this I mean, the frustration you have in the beginning, and after a success experience you feel even more motivated to learn the new skill. 
But with coding I found it easier because, I from the beginning had an idea of how it worked. 
I hope to become even more skilled with coding with more experience and more tries. 
Coding has some similarity with writing because you have to construct a sentence correctly for it to make sense. 
Getting the wright outcome comes with several tries and failure/ success, which in the end leaves you with knowledge about this new skill.   

**What is code and coding/programming practice means to you?**
For me coding means understanding the process behind programming a new project. 
It also means it gives me opportunity for job offers in the future - which is a big plus for me. 
Coding also means it gives me a space for new creativity and for a new way of expressing myself. 
The opportunity to learn coding, is something that is very important now days, because of the way the world going.    


My mini extercize:
https://cdn.staticaly.com/gl/LauraDevantie/ap2019-mini_ex/raw/master/Mini%20ex%201/p5/empty-example/index.html





Laura Trinnerup Devantie