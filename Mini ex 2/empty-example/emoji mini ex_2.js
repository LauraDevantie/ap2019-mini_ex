var r;
var g;
var b;

function setup (){
createCanvas(1500,800);
let value = 0;
frameRate(10);

}

function draw () {
background(225,182,161);


//Smiley emoji
noStroke();
fill(245, 202,24);
circle(240,200,100);
fill(0);
circle(190,170,10);
circle (290,170,10);
arc(240, 230, 90, 100, radians(0), radians(180));



//transgener Smiley
noStroke();
fill(245, 202,24);
circle(1000,400,100);
//eyes
fill(0);
circle(960,370,10);
circle (1040,370,10);
//hat
textSize(150);
text('🎩',896,310);
//mouth
textSize(70);
text('👄', 955, 460);
//eyelashes
stroke(0);
strokeWeight(2)
line(960, 370, 980, 350);
stroke(0);
strokeWeight(2)
line(960, 370, 965, 345);
stroke(0);
strokeWeight(2)
line(960, 370, 945, 345);
stroke(0);
strokeWeight(2)
line(960, 370, 930, 355);

//change color of Smiley
if (frameCount % 5 ==0){
r= random (0,240);
g= random (0,130);
b= random (0,50);
}
noStroke();
fill(r,g,b);
  circle(240,200,100);
  fill(0);
  circle(190,170,10);
  circle (290,170,10);
  arc(240, 230, 90, 100, radians(0), radians(180));

//flower emoji
translate(580, 200);
stroke(255,255,51);
strokeWeight(5);
  fill(214,132,235);

  var step = frameCount % 20;
  var angle = map(step, 0, 20, 0, TWO_PI);
  var cos_a = cos(angle);
  var sin_a = sin(angle);
  translate(50, 50);
  // Equivalent to rotate(angle);
  applyMatrix(cos_a, sin_a, -sin_a, cos_a, 0, 0);
for (let i = 0; i < 10; i ++) {
rotate(PI/5);
  ellipse(0, 50, 20, 180);

}
//second change of color
if (mouseIsPressed) {
translate(1, 1);
stroke(0,128,255);
strokeWeight(5);
  fill(128,255,0);
for (let i = 0; i < 10; i ++) {
rotate(PI/5);
  ellipse(0, 50, 20, 180);
}
}


}
