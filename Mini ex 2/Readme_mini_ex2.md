I have created three emojis: a "normal" emoji that chances skin-color, a flower that spins and chances color when you press the mouse and a transgender emoji. 
I have learnt how to get a figure to chance color slowly, getting a figure to spin in a controlled way and overall i have learnt how to create something visual - so your ideas get visualized. 

What i have learnt from reading the weeks assigned reading is that what you program can have a larger impact than you might think. 
A simple thing as an emoji can start a larger discussion about race, sex and equality. 

My first emoji is a statement against facebooks update- that you can change the skins of an emojis color. My emojis chances skin color constantly - so every color is represented. 
Its a statement against the need for a emoji to have a skin-color. This emoji has a cultural context in the terms of identity, race and identification. 
This emoji was very simple to make, but the thing that was difficult was getting it to chance color in a slow rate. 

My second emoji is a turning flower, that chances color when you press the mouse. This emoji does not have a cultural context. 
The emoji was a challenge to my self in terms of getting it to spin in a certain way and getting it to chance color when you press the mouse. 

My third emoji is a transgender emoji. I created this because I think there is a need for a transgendered emoji - so everyone can identify with an emoji. 
This emoji was difficult to create because i had to incorporate some text - to get the hat and lips on the emoji. 

Screenshots of my code:

[Screenshot](Kode1.png)
[Screenshot](Kode2.png)
[Screenshot](Kode3.png)

Screenshot of my program:

[Screenshot](mini ex_2billede.png)

My mini exercise:
https://cdn.staticaly.com/gl/LauraDevantie/ap2019-mini_ex/raw/master/Mini%20ex%202/empty-example/index.html

