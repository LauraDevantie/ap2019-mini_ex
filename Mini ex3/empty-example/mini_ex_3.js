let font,
fontSize= 40;
var words = ["11% of people are left handed", "a bear has 42 teeth", "rabbits like licorice", "reindeer like bananas", "Elvis's middle name was Aron", "cats spend 66 % of their life aleep"];

var index = 0;


function preload (){
  font = loadFont ('assets/orange juice 2.0.ttf');
}


function setup() {
createCanvas(1000,600,WEBGL);
textFont(font);
    textSize (width/14);
  textAlign(CENTER);
  frameRate(10);


}

function draw () {
background (150,50,140);

//The 3d figure turning
push();
r= random (0,255);
g= random (0,255);
b= random (0,255);
fill (r,g,b);
rotateX(frameCount * 0.10);
torus(120, 12);
pop();


//secound 3d figure turning
push();
rotateY(frameCount* 0.10);
torus (120,12)
pop();

//Text in the middle
fill(0);
textSize (30);
text("Loading", 10,20);


//points
fill(0,0,0,0)
circle (65,20,4,20);
fill(0,0,0,200);
circle (75,20,4,20);
fill(0);
circle (85,20,4,20);



//text on the screen
fill(0);
textSize (32);
text('Did you know ' + words[index],12,200);
text('Click the mouse to learn something new', 12,-200);

//So all the words in the array gets used and it starts over
if (index == words.length){
  index= 0;
}


}
//function so that the facts change when the mouse is pressed
function mousePressed(){
index = index + 1;

}
