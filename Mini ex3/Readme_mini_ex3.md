Screenshot of my program:
[Screenshot](Skærmbillede.png)

 
**What are the time-related syntaxes/functions that you have used in your program? and why you use in this way?** 
**How is time being constructed in computation (can refer to both reading and your process of coding)?**

I have created a throbber with several element in it. There used a syntax to create the text, which was difficult because the normal text function can't work, 
when you have a 3d figure. But i downloaded and incorporated a font, so that i could run the text. There is used a syntaxt to create the 2 3d figures that rotates. 
There is also used a syntax for the 3 dots - with different and fade  colors. And there is used an array to create the loop with the text. 
I wanted to make the three dots near 'loading' to be shown one at the time- but I couldn't get it to work - so that is something i could like to experiment with next time. 
I have only incorporated  6 random facts so that I could show how the program is looping - turning back to the first fact when the last one is shown. 


I have created a throbber where the time related syntax is a loop - that runs over and over aging, when you press the mouse. 
In this, like i said, i used an array to help get the text to chance between every mouse press. I have thought i lot about the discussion we had in the class - 
that you are becoming to impatient now days - that everything has to be fast and we have to entertained all the time. It's this aspect of time, 
that i have thought about when programming this mini exercise. I have created a way, so that people (hopefully) will be entertained with waiting - 
that's why I incorporated the random - useless facts at the bottom. 
I have chosen it to be useless facts - kind of like a statement towards the assigned text and the class discussion - that sometimes it's okay to wait and be bored. 
So hopefully either people will se this- or maybe they just think it is some weird random facts. 
	
**Think about a throbber that you have encounted in digital culture e.g streaming video on YouTube or loading latest feeds on Facebook or waiting a ticket transaction,**
**what do you think a throbber tells us, and/or hides, about? How might we think about this remarkable throbber icon differently?**

I think encountering a throbber is a 'arrg not this aging' experience. We know that the throbber is telling us, that something is working and that it will be a minute or more. 
But we don't know what is going on behind the throbber - what is working, is it working and how long will this take. 
Maybe we could think differently about the throbber if we new more about the work, that goes behind the throbber - then maybe people won't get the 'argg not this aging' 
feeling every time they see the throbber. They would know what is working or if it just is the webpages that does that, because it was programed to it. 


Link to my program:
https://cdn.staticaly.com/gl/LauraDevantie/ap2019-mini_ex/raw/master/Mini%20ex3/empty-example/index.html