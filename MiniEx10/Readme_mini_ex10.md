**Group two flowcharts**

![Screenshot](Skærmbillede.jpg)
![Screenshot](Skærmbillede2.jpg)


**How is these flowcharts different from the one i made?**
The difference between my own flowchart and the two I have made in my group is that my flowchart is more detailed. There are more elements to that flowchart and it is not as simple as my group one is. This is because the group flowchart is only ideas, there is no clear program, so a lot of the things and element such as buttons and so on is not decided yet. In my own program there is more thoughts put into the users experience with the program. In my flowchart I could focus more on the flow of the program and the flow a user goes through when interacting with the program. The group flowchart is more describing the program not so much the different flows a user could go through. 

**Algorithms and flowcharts into a wider cultural context**
Algorithms represented in a flowchart can help people understand and learn coding. It can unite people together because it is no longer, just the developer that can understand coding, but also the other people in the project. It can maybe even prevent that a program can be misused and use in harms way. This is due to that fact that more people would be able to get involved in the programming process and there for see a new point of view on the program. This can also be used to make a program even better, now that more people are able to understand the works in making the program. People are able to understand this due to the fact that a flowchart is a "programmers blueprint" like they say in the text The multiple meanings of a flowchart by Nathan Ensmenger.  


**Individual flowchart**
Flowchart over my mini ex 8


<img src="Flowchart_over_mini_ex_8.png" alt="Flowchart_over_mini_ex_8" width="250"/>


I choose to draw a flowchart to my mini exercise 8. In my mini exercise 8 I worked together with Nina on this project, where we created a tree made out of lyrics from the manuscript from the move 'Forrest Gump'. 
I choose to focus my flowchart on the users interaction with the program. I choose to focus more on the interactive part instead of the technical part, because I think that would be essayer for people, that have not seen the program before, to understand the program. 
I choose to implement some technical part in the flowchart, to explain what happens, when the user presses the mouse. 
As you can see in the flowchart the program has to possibilities. It can keep going in circle, with creating a new tree, or stop at any moment, if the user does not want to continue exploring the program. 

What might be difficult when drawing a flowchart is making it simple but still show what happens when the user interacts with the program. Some aspects that you have to consider when drawing a flowchart is, how technical you want the flowchart to be. But also be aware that you are drawing what the program can do - and not what you want it to do. With a flowchart you want a real representation of the reality of your program, not the ideal version of the program. 
Link to my mini ex 8

[Mini Ex8](https://gitlab.com/LauraDevantie/ap2019-mini_ex/blob/master/miniex_8/Readme_mini_ex8.md)


Link to the program [RunMe](https://glcdn.githack.com/LauraDevantie/ap2019-mini_ex/raw/master/miniex_8/miniex_8/index.html)