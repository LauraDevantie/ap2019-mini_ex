First picture is the program before you press the mouse, second picture is after you press the mouse. 

![Screenshot](Skærmbillede.PNG)
![Screenshot](Skærmbilledeskov.PNG)

*The code and the Collab:*
This week I collaborated with Nina on your mini exercise. 
Our program is a tree (a Forest) that is made up by the manuscript from Forrest Gump. 
When you press the mouse the tree shifts and there is created a new tree that looks different every time you press the mouse. 
At the bottom we added some grass and at the top we created a sun and a cloud so it would be more obvious that the text creates a tree. 

The code for the tree we got from a example we found on p5.js examples. We altered the code so that the lines, that was there before, 
got switched out with text instead. To insert the text we created a JSON files, that we could call. This way the text on the branches is
random when every you press the mouse and the code is more organized because all the data used to create the text is captured in the JSON file. 

The smaller trees are images that Nina uploaded to the program. We wanted there to be more that just one tree because of the reference to Forrest gump.

It was a bit difficult getting the JSON file loaded in our program, but we got help from Ann and in the end it all worked out. 
We also had some difficulties getting the smaller trees to be there all the time and not just when you press the mouse, but unfortunately we did not make that happen.

The collaborative process worked out good, we started working together at UNI and got most of the job done together. 
The only thing we did separately was Nina uploaded the pictures of the trees and altered the text, and I created the sun and the clouds and the text in the cloud. 

*Conceptual thoughts on coding and language in terms of this project:*
In terms of the question of the relationship between code and language in our project, it refence to the conversation we had in class last Tuesday. That what you see, 
is not always that you get. That you see a tree, but what you really are presented with is the manuscript of Forrest gump. We choose Forrest gump because it could fit very well,
because we had a tree in out program. In this way, we captures the aspect of 'What you see, is not always what you get'. And the program looks very simple, but behind the screen 
there is a lot of code that had gone into the making. This is not just the source code but also the JSON files. So the program is not so simple after all. This also addresses the
point that there is a difference between the source code and the executing code - because there is an added element with the JSON file. 


In this question between, code and language, i think the term or expression 'Secondary notation' is valid to look at. This is because there is two different purposes with a
"digital artefact". There is the primary purpose executed by a computer - executing the code and presenting with a visual outcome. And then there is the secondary purpose 
that is the interpretation of a human. When presented with a digital artefact, people rarely look at the code and interpret that, people look at the visual outcome and interpret 
that. This is valid in out project, because there is an aspect of the design, that you are  not presented with if you just read the code. There is the aspect of the literature
that is chosen for this program. 

In this we can also include Kittler in the answering of the question. Because Kittler talk about a need for new literature that includes natural and artificial languages. 
This could be done by a digital artefact that incorporates literature into the art. This is not saying that our program does this, because there is only used natural language, 
but i could be possible to incorporate artificial language in some way.

Mit program [RunMe](https://glcdn.githack.com/LauraDevantie/ap2019-mini_ex/raw/master/miniex_8/miniex_8/index.html)