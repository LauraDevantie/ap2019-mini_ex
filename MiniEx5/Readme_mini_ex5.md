# Ap2019-Mini_ex
![Sreenshot](skærmbillede.png)

In the program, the first thing you hear is a voice speaking. Except that there are 4 circles, a heart, a button and 5 sliders. 
To each circle there is a connecting slider that adjusts something to the circle. To circle in the upper left corner the slider adjust the circles size. 
The circle in the upper right corner, the slider adjusts the circles height. The circle at the button left corner, 
the slider adjust the circles color while also adjusting the color of the circle in the lower right corner. 
The slider in the middle of the two circles in the button also adjust the color. In the middle of the screen there is a heart and a button. 
It you press the button the heart is suppose the turn red (which it does sometime, sometimes not). To create this I used syntax to create the circles, 
the slider, the create.button, to create the heart I used syntaxes point and curveVertext. I also added another library - the speech library, 
so I could incorporate a speaker in the begging of the program. 

I didn't get to finish my program how i wanted to, there were a lot of technical difficulties that prevented me from doing it. 
That means that the concept of the work isn't complete. The concept was suppose to be a modern love letter kind a thing, but 
I couldn't get the text inserted and the heart will only fill with red sometimes when you press the button. 
But from the original program I kept the 4 circles and the random color changes. I gave the circles a purpose and added effect 
that we have learnt after on ex the sliders and the added sound. 

I took out the rotating box, that didn't follow the mouse properly. I could have kept it and made the box follow the mouse, 
but i have done that in an other exercise so I thought it could take it out without it leaving a big blank hole. With the box out i created the heart, 
which could be done better and properly with a different syntax, but aging I wanted to try something new. 

The feedback that I incorporated in my program was Maja's comment on adding // to your code, that is is more reader able to others, 
which is something I do on all my exercises know. 


link
https://gl.githack.com/LauraDevantie/ap2019-mini_ex/raw/master/MiniEx5/p5/empty-example/index.html