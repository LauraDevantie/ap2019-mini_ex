
var slider1
var slidervalue;
var slider2 = 45
var slider3
var slider4
let face;
var clmtrackr
let button

let font
function preload(){
}


function setup() {
 createCanvas(windowWidth,windowHeight, WEBGL);
font= loadFont('empty-example/assets/orange juice 2.0.tff');
textFont(font);
textSize(14);
textAlign(CENTER);// the text part diddn't work in the end

frameRate(10);

  var foo = new p5.Speech(); // speech synthesis object
  foo.speak('hi there, please show some love'); // say something


//creating a slider the one top left conor
slider1= createSlider(0,255,100);
slider1.position(300,250);
slider1.style('width','80px');

//Slider top right conor
slider2= createSlider(0,255,100);
slider2.position(1150,250);
slider2.style('width','80px');

//slider left conor
slider3= createSlider(0,255,100);
slider3.position(300,650);
slider3.style('width','80px');

//slider right conor
slider4= createSlider(0,255,100);
slider4.position(1150,650);
slider4.style('width','80px');

//last color slider
slider5=createSlider (0,255,100);
slider5.position (700,650);
slider5.style('width','80px');


//create a button
button= createButton('Give some love');
button.position(700,600);
button.mousePressed(kærlighed);

//didn't work
fill(0);
textSize(15);
text("How big is your love", 10,300);

}

function draw() {
   background(255,204,0);
//the small circles on the screen
fill(random(204),70, 130);
circle(-430, -200,slider1.value (), slider1.value()); //changes the circles size


ellipse(430,-200,45,slider2.value());// the slider changes to ellipse size

push();
fill(slider3.value(), slider5.value ());
circle(-430,200,25);
// making the color change when using the slider
const r= slider4.value();
const g= slider3.value();
const b= slider5.value();
fill(r,g,b);
circle(430,200,25);
pop();


//trying to create a heart:
strokeWeight(5);
point(-35,-25);
point(-65,-35);
point(-100,-25);
point(-120,0);
point(-120,20);
point(-100,50);
point(-60,80);
point(-30,100);
point(0,115);
point(30,100);
point(60,80);
point(100,50);
point(120, 20);
point(110, -20);
point(-15, -5);
point(65, -35);
point(20, -25);

noFill();
beginShape();
curveVertex(-35,0);
curveVertex(0,0);
curveVertex(-35,-25);
curveVertex(-35,-25);
curveVertex(-65,-35);
curveVertex(-100,-25);
curveVertex(-120,0);
curveVertex(-120,20);
curveVertex(-100,50);
curveVertex(-60,80);
curveVertex(-30,100);
curveVertex(0,115);
curveVertex(30,100);
curveVertex(60,80);
curveVertex(100,50);
curveVertex(120,20);
curveVertex(110,-20);
curveVertex(65,-35);
curveVertex(20,-25);
curveVertex(-15,-5);
curveVertex(-35,0);
endShape();

}

function kærlighed() {
fill(255,0,0);
beginShape();
curveVertex(-35,0);
curveVertex(0,0);
curveVertex(-35,-25);
curveVertex(-35,-25);
curveVertex(-65,-35);
curveVertex(-100,-25);
curveVertex(-120,0);
curveVertex(-120,20);
curveVertex(-100,50);
curveVertex(-60,80);
curveVertex(-30,100);
curveVertex(0,115);
curveVertex(30,100);
curveVertex(60,80);
curveVertex(100,50);
curveVertex(120,20);
curveVertex(110,-20);
curveVertex(65,-35);
curveVertex(20,-25);
curveVertex(-15,-5);
curveVertex(-35,0);
endShape();

}
