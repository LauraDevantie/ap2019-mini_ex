let oste= [];
let ghost;


function setup() {
createCanvas (windowWidth, windowHeight);
//background (255);

//adding the sprite
ghost=createSprite(50,50);
ghost.addAnimation('round','ghost_walk0002.png');


for(var i=0; i<20; i++){ //adding a new cheese with random sizes
  oste[i]=new ost(random(3,10),0,random(0,windowHeight),random(25,100));
}

}

function draw() {
background (255);

//text in the top of the program
fill(0);
textSize(20);
text('Get the ghost to eat all the cheese',600,50);

//making the sprite move with the mouse
ghost.position.x=mouseX;
ghost.position.y=mouseY;

drawSprites(); //making the sprite appear on the screen

//removing a cheese everytime the mouse is over the object.
for (let i=0; i<oste.length; i++){
  if(oste[i].contains(mouseX,mouseY)){
    oste.splice(i,1);
  }
}
for (let i=0; i<oste.length; i++){ //adding a cheese and making it move and show
  oste[i].move();
  oste[i].show();
}

}

function mousePressed () { //when the mouse is over the object the object is removes from the program
  for (let i=0; i<oste.length; i++){
    if(oste[i].contains(mouseX,mouseY)){
      oste.splice(i,1);
  }
}
}
class ost { //chesse class
constructor (speed, xpos, ypos, size) {
this.speed= speed;
this.x=xpos;
this.y=ypos;
//this.pos= new createVector(xpos,ypos);
this.size= size;
}

contains(px,py){ //if the mouse is over the cheese true if not false
  let d=dist(px,py, this.x, this.y);
  if (d<this.size){
    return true;
  }else {
    return false;
  }
}
move() {  //moving behaviors
  this.x+=this.speed;  //x pos and speed
  if (this.x > width +3){
    this.x=0;
  }
}

show(){ //show the object. 
  fill(254,227,34);
arc(this.x, this.y, this.size, this.size, this.size, PI + QUARTER_PI, PIE);
}

}
