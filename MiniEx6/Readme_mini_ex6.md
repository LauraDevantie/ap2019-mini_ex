![Screenshot](Skærmbillede.png)

The game works with cheese flowing from the left side to the right side. With the ghost - that follows the mouse, you have to "eat" the cheeses to win. 
To create the program i created a class called 'ost' that contains all the information's about the object - the position, the size and color. 
I also incorporated a sprite, that came along the p5 play program. I loaded the sprite and made it follow my mouse. Then I made a  function that looks 
if the mouse is over the chesses(the small objects), of the mouse was over the object, the object had to be removed. 
This was the difficult thing to program with my game. It took me a long time figure out how to do it, but in the end it came out okay. 
I really wanted to incorporate, that when you made the ghost eat all the chesses, there would appear text that statet that you won, but I did not succeed in making this
- so maybe next exercise I will try to make that. 


Object oriented programming is a way of programming where you have organized you code into objects and classes. Like it says in the text:
 "Objects, in object orientation, are groupings of data and the methods that can be executed on that data, or stateful abstractions" page 1. 
 It is a way of making the code more complex and also more simple. It contains the data in smaller sets of data, so you don't have to write the code aging 
 - 're-use' the code, like they say in the text. 
This way you also loose some risk with writing the wrong code- because you just have to write it correct the first time. 

A digital example of how complex details and operator are being abstracted is almost any game when programmed. 
Example could be a simple game like pac man. Here there is focus on the certain things and other things are not necessary to look at. You have to be very 
precise when describing what you want - and in pac man it is the movement, what the objects can do and how they look like - not way the pac man is eating 
the dots on the screen. A example that does everything; looks at every little detail, the movement, the look and the feelings/ personality of the
programmed object is sims - you create a person and also implicates some "feelings" in some sort, at least you program how you want their personality to be like. 


link:
https://glcdn.githack.com/LauraDevantie/ap2019-mini_ex/raw/master/MiniEx6/empty-example/index.html
