var slider //to the slider
var slidervalue;
var capture
var button1
var button
var knap
var balls
var slide
let img
let hej
let billede
var kort


function setup() {
img= loadImage('profilbillede.PNG');
billede= loadImage('kort.PNG');

  var c = createCanvas(windowWidth, windowHeight);
  c.position(0,75); //making so that the webcam could be shown on the canvas



//creating the button that follows the mouse
button= createButton ('I am watching you');
button.position(500,300);
button.mousePressed(webcam);
capture= createCapture(VIDEO);
capture.hide();
background(220,217,216);

//data button
knap= createButton ('Click to see your personal data');
knap.position(50,350);
knap.mousePressed (lifestory);
knap.size(250,40);


//mood button
button1=createButton('Mood');
button1.position(1200,200);
button1.mousePressed(but);

//like and dislike button
hej=createButton ('Likes and dislikes');
hej.position(1200,450);
hej.mousePressed (likes);
hej.size(200,40);


//credit cart button
kort=createButton ('Credit cart information');
kort.position(50,620);
kort.mousePressed(inf);

//Creating a slider
slider= createSlider(0,255,100);
slider.position(700,250);
slider.style('width','80px');

}

function draw () {

  //slider value
 slidervalue=slider.value();

  //profil image
  image(img,100,120,150,150);


//text in profile
fill(0);
textSize (15);
text ('Remeber to update your status',50,350);
text('Last update: 30sek.',50,370);


//Text above slilder
textSize (25);
text('Current mood',660,150);

//Creating a smiley
//face
noFill();
circle (730,250,50,20);
//eyes
fill(0);
circle(705,240,5);
circle(755,240,5);

//mouth
noFill();
//sad mouth
push();
if(slidervalue<85){
  arc(730, 290, 65, 80, PI,TWO_PI);
}else{
  stroke(220,217,216)
  arc(730, 290, 65, 80, PI,TWO_PI);
}
pop();
//nutral mouth
push();
if(slidervalue>85 && slidervalue<170){
fill(0);
line(760, 270, 700, 270);
}else{
  stroke(220,217,216);
  line(760, 270, 700, 270);
}
pop();
//happy mouth
push();
noFill();
if(slidervalue>170){
  arc(730, 250, 80, 80, 0, PI);
}else{
  stroke(220,217,216)
    arc(730, 250, 80, 80, 0, PI);
}
pop();



//Getting button to follow the mouse
button.position(mouseX,mouseY+75);

//Blue facebook color above
fill(19,93,217);
rect(0,0,1525,95);

//rectangle with information
push();
strokeWeight(2);
noFill();
rect(25,100,360,600);
pop();

}
//information and function of the button 'likes and dislikes'
function likes() {
fill(0);
textSize(20);
text('You like:',1200,450);
text('You dislike:', 1300,450);
//likes
textSize(15);
text('Food',1200,470);
text('Dogs',1200,490);
text('Music',1200,510);
text('Walks',1200,530);
text('Instagram',1200,550);
text('Netflix',1200,570);
text('Working out',1200,590);

//dislikes
text('People that chew loudly',1300,470);
text('Vomit',1300,490);
text('Public transportation',1300,510);
text('Shopping for shoes',1300,530);
text('Advertisements',1300,550);
text('Snakes',1300,570);
text('Tequila',1300,590);

}


//Webcam
function webcam (){
capture.position(500, 300);
capture.show(); //getting the webcam to show when you push the button
}


//information in the buttin 'click to see your personal data'
function lifestory () {
  fill(0);
textSize(20);
text('This is your personal data:',50,400);
textSize (17);
text('Full name: Laura Trinnerup Devantie',50,420);
text('Birthday: 21 november', 50,440);
text('Adress: Jordbrovej 31 1,2',50,460);
text('Relationshipsstatus: In a relationship',50,480);
text('Last lokation: Julsøvej 1, 8240 Risskov', 50,500);
text('Det er jo det, jeg vil bare ikke tage afsted alene',30,540);
text('Last message sendt',50,520);
}

//the credit cart information
function inf() {
  image(billede,50,580,200,120);
}


//Information in the button 'Mode tracker'
function but() {
  fill(217,79,19);
  textSize (15);
  text('Monday: 40% happy',1180,180);
  fill(19,217,78);
  text('Tuesdag: 80% happy',1180,210);
  fill(206,19,217);
  text('Wensday: 67% happy', 1180,240);
  fill(19,216,217);
  text('Thursdag: 45% happy',1180,270);
  fill(224,162,10);
  text('Friday: 90% happy',1180,300);
  fill(0);
  text('*Your mood is captured through audio and visual sensors',1100,350);
}
