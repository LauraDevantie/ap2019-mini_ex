![Screenshots](Skærmbillede1.png)
![Screenshots](Skærmbillede2.png)

**Describe about your sketch and explains what have been captured both conceptually and technically. How's the process of capturing?**
In my project I have tried to capture a various of new technical things, that i haven't tried before. I have created a persons personal "page"
- a page where all your information is gathered. 
 In regards of the technical elements in my project I have created three buttons. 
 The first button 'Click to se your personal data' contains personal information about the user. The second button 'Mood' tracks the users emotions through data. 
 The third button 'I am watching you' follows the mouse, when you press that mouse the webcam turns on.
 The fourth button 'Credit cart information' shows you a picture of the persons credit cart information which is shown in a picture of a credit cart 
 (a picture that i found on my banks webpage, so it is not a real credit cart with actual credit cart information on it). 
 The last button 'Likes and dislikes' shows a list of the persons likes and dislikes. 
I have also created a 'header' for my page, added a "profile picture" for the persons profile. 
There is also a emoji, where you can show your emotion- when you use the slider the mouth of the emoji chances. 


I have tried to show this weeks theme 'Data capturing' with the thought that the internet contains so much of your data and personal information, 
that the web could hold webpage donated to every person. I tried to show this by creating a "private page" for a person. 
Not private in the sense that, because it is about me, it is only me who can see it, but that it is private because it is personal information about me.
On this page all the data that is captured about me is displayed. My idea is that on this page you can se everything that the internet knows about you 
through the data that is being captured about you. So this page would be a lot more detailed if it was real. 
The information that is being captured and displayed on the page, is name, address, relationships status, last noted location, last message sent,
credit cart information, your likes and dislikes, your mood and a emojis where you can show your emotion of the day. 
The button with the caption 'I am watching you' is thought to be like "big brother watching you". I have shown this by making the webcam being turned on when you push the button. 
I have made the data captured on the page realistic but also non realistic - with the mood tracker. 

**Together with the assigned reading and coding process, how might this ex helps you to think about or understand the data capturing process in digital culture?**
I think the reading assignment and making my own program helped me se how much information the web could know about me.
I was already aware of the way webpages capture information about you to personalize advertisement to you.
But a new thing I learn from this weeks assignment, was Facebook role in this. There thought that making the web more social is a positive view i think. 
But in their way of making it more social sound like making the web more informed on the people using the web - so they can customize the web to each person. 
In this digital culture we are being forced to accept cookies - so we can read or access a webpage, giving the webpage access to information about you. 
This is something I think a lot of people don't think about. I didn't think about the thought that login with your Facebook account gives the new webpage
access to the information that Facebook has on you, this is a new way seeing Facebooks statement that they want the web to be "more social". 
I tried to incorporate these thoughts in my program.  



Link to my program:
https://glcdn.githack.com/LauraDevantie/ap2019-mini_ex/raw/master/MiniEx4/empty-example/index.html